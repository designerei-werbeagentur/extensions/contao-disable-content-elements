# Contao-Disable-Content-Elements

## Function & usage

This extension for Contao Open Source CMS allows you to disable content elements within the `config.yml`.

## Configuration

This is an exemplary entry. You have to define the content elements, you want to disable, by adjusting the `config.yml`. [Here](https://github.com/contao/core-bundle/blob/4.4.20/src/Resources/contao/config/config.php#L139) you can see the structure and the naming of the default contao content elements.

```yml
contao_disable_content_elements:
  foo:
    - bar
    - quz
```
