<?php

declare(strict_types=1);

namespace designerei\ContaoDisableContentElementsBundle\DependencyInjection;

use designerei\ContaoDisableContentElementsBundle\EventListener\DisableContentElements;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Definition;


class ContaoDisableContentElementsExtension extends Extension
{

    public function getAlias(): string
    {
      return 'contao_disable_content_elements';
    }

    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $optionsDefinition = $container->getDefinition(DisableContentElements::class);
        $optionsDefinition->setArgument(0, $config);
    }
}
