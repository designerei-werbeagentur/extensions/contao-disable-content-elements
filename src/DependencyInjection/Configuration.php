<?php

declare(strict_types=1);

namespace designerei\ContaoDisableContentElementsBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('contao_disable_content_elements');

        // Keep compatibility with symfony/config < 4.2
        if (method_exists($treeBuilder, 'getRootNode')) {
            $rootNode = $treeBuilder->getRootNode();
        } else {
            $rootNode = $treeBuilder->root('contao_disable_content_elements');
        }

        $rootNode
            ->useAttributeAsKey('name')
            ->normalizeKeys(false)
            ->arrayPrototype()
                ->scalarPrototype()->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
