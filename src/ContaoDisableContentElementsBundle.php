<?php

declare(strict_types=1);

namespace designerei\ContaoDisableContentElementsBundle;

use designerei\ContaoDisableContentElementsBundle\DependencyInjection\ContaoDisableContentElementsExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ContaoDisableContentElementsBundle extends Bundle
{
    public function getContainerExtension(): ContaoDisableContentElementsExtension
    {
        return new ContaoDisableContentElementsExtension();
    }
}
