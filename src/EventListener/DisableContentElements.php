<?php

namespace designerei\ContaoDisableContentElementsBundle\EventListener;

use Contao\CoreBundle\ServiceAnnotation\Hook;

/**
 * @Hook("initializeSystem")
 */
class DisableContentElements
{
    /**
     * Note: This must run before Contao's GlobalsMapListener (255) that
     *       adds our custom fragment controllers.
     *
     * @Hook(initializeSystem, priority=256)
     */

    private array $config;

    public function __construct(array $config)
    {
        $this->disabledElements = $config;
    }

    public function __invoke(): void
    {
        // elements to hide
        $disabledElements = $this->disabledElements;

        // merge configuration
        $container = &$GLOBALS['TL_CTE'];
        foreach ($container as $key => &$category) {
            if (!isset($disabledElements[$key])) {
                continue;
            }

            $category = array_diff_key($category, array_flip($disabledElements[$key]));
        }
    }
}
